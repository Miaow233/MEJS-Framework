declare namespace console {
  function log(...args: any[]): void
  function info(...args: any[]): void
  function warn(...args: any[]): void
  function error(...args: any[]): void
}
